#include "bmp.h"

enum read_status from_bmp(FILE* in, struct image* img) {

    if (!in) return READ_INVALID_FILE;
    if (!img) return READ_INVALID_IMAGE;

	struct bmp_header h;

	if (!fread(&h, sizeof(struct bmp_header), 1, in))return READ_INVALID_HEADER;
	if (h.bfType != 0x4D42 && h.bfType != 0x424D) return READ_INVALID_SIGNATURE;

    const uint32_t width = h.biWidth;
    const uint32_t height = h.biHeight;

	*img = create_image(width, height);

    if (!img->data) {
        return READ_MEMORY_ALLOCATION_ERROR;
    }

    const uint32_t p = padding_size(width);

    for (uint32_t i = 0; i < height; i++) {
        if (fread(img->data + width * i, pixel_size(), width, in) != width) {
            delete_image(img);
            return READ_INVALID_BITS;
        }
        if (fseek(in, p, SEEK_CUR)) {
            delete_image(img);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

struct bmp_header header_sample(const uint32_t width, const uint32_t height, struct image const* img) {
    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = sizeof(struct bmp_header) + padding_size(width) + image_size(img),
            .bfReserved = RESERVED,
            .bOffBits = OFF_BITS,
            .biSize = SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = image_size(img),
            .biXPelsPerMeter = X_PELS_PER_METER,
            .biYPelsPerMeter = Y_PELS_PER_METER,
            .biClrUsed = CLR_USED,
            .biClrImportant = CLR_IMPORTANT,
    };
}

enum write_status to_bmp(FILE* out, struct image const* img) {

    const uint32_t width = img->width;
    const uint32_t height = img->height;
    struct bmp_header h = header_sample(width, height, img);

    if (!fwrite(&h, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    uint8_t m[3] = {0};

    for (uint32_t i = 0; i < height; i++) {
        if (!fwrite(img->data + width * i, width * pixel_size(), 1, out)) return WRITE_ERROR;
        if (!fwrite(m, padding_size(width), 1, out)) return WRITE_ERROR;
    }
    return WRITE_OK;
}
