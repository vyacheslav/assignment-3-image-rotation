#include "image.h"

size_t pixel_size(void) { return sizeof(struct pixel); }

uint32_t padding_size(const uint32_t width) {
    return 4 - pixel_size() * width % 4;
}

uint32_t image_size(struct image const* img) {
    return img->width * img->height * pixel_size();
}

struct image create_image(const uint32_t width, const uint32_t height) {

    struct image img;

    img.width = width;
    img.height = height;
    img.data = (struct pixel*)malloc(width * height * pixel_size());

    return img;
}

void delete_image(struct image* img) {
    if (img && img->data) {
        free(img->data);
        img->data = NULL;
    }
}
