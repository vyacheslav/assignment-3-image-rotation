#include "image.h"
#include "bmp.h"
#include "rotate.h"
#include <stdio.h>

int main( int argc, char** argv ) {

    if (argc != 3) {
        fprintf(stderr, "Wrong number of arguments!\n");
        return 1;
    }

    FILE *in = fopen(argv[1], "rb");

    if (!in) {
        fprintf(stderr, "Can't open input file\n");
        return 1;
    }

    FILE* out = fopen(argv[2], "wb");

    if (!out) {
        fclose(in);
        fprintf(stderr, "Can't open output file\n");
        return 1;
    }

    struct image img;

    if (from_bmp(in, &img)) {
        fclose(in);
        fclose(out);
        delete_image(&img);
        fprintf(stderr, "Failed to read bmp\n");
        return 1;
    }

    struct image rotated = rotate(&img);

    if (to_bmp(out, &rotated)) {
        fclose(in);
        fclose(out);
        delete_image(&img);
        delete_image(&rotated);
        fprintf(stderr, "Failed to convert to bmp\n");
        return 1;
    }

    delete_image(&img);
    delete_image(&rotated);
    fclose(in);
    fclose(out);

    return 0;
}
