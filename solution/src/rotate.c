#include "rotate.h"

struct image rotate(const struct image* source) {

    const uint32_t width = source->width;
    const uint32_t height = source->height;

    struct image rotated = create_image(height, width);

    if (!rotated.data) {
        return rotated;
    }

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            rotated.data[(j + 1) * height - i - 1] = source->data[i * width + j];
        }
    }

    return rotated;
}
