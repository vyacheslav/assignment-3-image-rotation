#pragma once

#ifndef BMP_H
#define BMP_H

#define TYPE 0x4D42
#define RESERVED 0
#define OFF_BITS sizeof(struct bmp_header)
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define X_PELS_PER_METER 2834
#define Y_PELS_PER_METER 2834
#define CLR_USED 0
#define CLR_IMPORTANT 0

#include "image.h"
#include "statuses.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#pragma pack(pop)

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);

#endif
