#pragma once

#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"

struct image rotate(const struct image* source);

#endif
