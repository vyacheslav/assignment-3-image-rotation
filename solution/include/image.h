#pragma once

#ifndef IMAGE_H
#define IMAGE_H
#include <malloc.h>
#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)

struct pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
};

#pragma pack(pop)

struct image {
	uint64_t width, height;
	struct pixel* data;
};

struct image create_image(uint32_t width, uint32_t height);

uint32_t image_size(struct image const* img);

size_t pixel_size(void);

uint32_t padding_size(uint32_t width);

void delete_image(struct image* img);

#endif
