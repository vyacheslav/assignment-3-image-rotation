#pragma once

#ifndef STATUSES_H
#define STATUSES_H

enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MEMORY_ALLOCATION_ERROR,
    READ_INVALID_FILE,
    READ_INVALID_IMAGE
};

#endif
